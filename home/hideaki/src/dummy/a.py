import os

def find_all_files(directory):
    for root, dirs, files in os.walk(directory):
        yield 'd ' + root
        for file in files:
            yield os.path.join(root, file)

for file in find_all_files('.'):
    print(file)
